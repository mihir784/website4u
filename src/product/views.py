import stripe
from .serializers import (
    OrderSerializer,
    ProductSerializer,
    CartSerializer,
    DiscountCouponSerializer
)
from rest_framework import permissions, viewsets
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from .models import Order, Product, CartItem, DiscountCoupon
from django.core.mail import send_mail

stripe.api_key = 'sk_test_51JklYYSC0dMKlMeiHvE4XVRzS0' + \
    'kI1cE4IGZBXzNAOwABQpMbCD3hAabKEZAAAQTPRrpM5qWzXyiUWqZvpvN3sCnp00UE8wfPzC'
# Create your views here.


class ProductAPIView(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class CartAPIView(viewsets.ModelViewSet):
    queryset = CartItem.objects.all()
    serializer_class = CartSerializer

    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        return self.request.user.cart.filter(is_ordered=False)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class PaymentView(APIView):

    def post(self, request, *args, **kwargs):
        order = request.user.cart.filter(is_ordered=False)
        token = request.data['stripeToken']
        discount = request.data['discount']

        amount = 0
        for item in order:
            amount += item.quantity * item.product.price
        amount -= discount
        amount = int(amount * 100)

        try:
            charge = stripe.Charge.create(
                amount=amount,
                currency="inr",
                source=token,
            )

            currOrder = Order.objects.create(
                user=request.user,
                total=amount/100,
                discount=discount
            )

            for item in order:
                item.is_ordered = True
                item.order = currOrder
                item.save()
            message = f"Your order on shop {request.user.shop.shop_name}" + \
                " Sponsored by Website4u is successfully submitted."
            if discount != 0:
                message += "\nThe sub-total for order is" + \
                    f" {currOrder.total+currOrder.discount} " + \
                    f"with discount of Rs. {currOrder.discount}."
            message += f"\nThe total for order is Rs. {currOrder.total}"

            send_mail(
                'Order Successful',
                message,
                'vivek.dubey.17@mountblue.tech',
                [request.data['email']],
                fail_silently=False,
            )
            return Response(status=HTTP_200_OK)

        except stripe.error.CardError as e:
            breakpoint()
            body = e.json_body
            err = body.get('error', {})
            return Response(
                {"message": f"{err.get('message')}"},
                status=HTTP_400_BAD_REQUEST
            )

        except stripe.error.RateLimitError as e:
            breakpoint()
            return Response(
                {"message": "Rate limit error"},
                status=HTTP_400_BAD_REQUEST
            )

        except stripe.error.InvalidRequestError as e:
            breakpoint()
            return Response(
                {"message": "Invalid parameters"},
                status=HTTP_400_BAD_REQUEST
            )

        except stripe.error.AuthenticationError as e:
            breakpoint()
            return Response(
                {"message": "Not authenticated"},
                status=HTTP_400_BAD_REQUEST
            )

        except stripe.error.APIConnectionError as e:
            breakpoint()
            return Response(
                {"message": "Network error"},
                status=HTTP_400_BAD_REQUEST
            )

        except stripe.error.StripeError as e:
            breakpoint()
            return Response(
                {
                    "message":
                    "Something went wrong. Please try again."
                },
                status=HTTP_400_BAD_REQUEST
            )

        except Exception as e:
            breakpoint()
            return Response(
                {
                    "message":
                    "A serious error occurred. We have been notifed."
                },
                status=HTTP_400_BAD_REQUEST
            )


class DiscountCouponAPIView(viewsets.ModelViewSet):
    queryset = DiscountCoupon.objects.all()
    serializer_class = DiscountCouponSerializer

    def retrieve(self, request, *args, **kwargs):
        try:
            discount_amount = DiscountCoupon.objects.get(
                coupon_code=request.path.split("/")[-2]
            ).discount_amount
            return Response(discount_amount, status=status.HTTP_200_OK)

        except Exception:
            return Response({
                "non_field_errors": [
                    "Invalid Coupon"
                ]
            }, status=HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        try:
            coupon = DiscountCoupon.objects.get(
                coupon_code=request.GET['coupon_code']
            )
            return Response({
                "non_field_errors": [
                    "Coupon Code Already in use"
                ]
            }, status=HTTP_400_BAD_REQUEST)

        except Exception:
            return Response(coupon, status=status.HTTP_200_OK)


class OrderHistoryView(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        return self.request.user.orders.all()
