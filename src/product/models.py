from django.db import models
from user.models import Shop, User


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(
        upload_to="product_images", blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class CartItem(models.Model):
    user = models.ForeignKey(
        User,
        related_name="cart",
        on_delete=models.CASCADE
    )
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    order = models.ForeignKey(
        "Order",
        related_name="order_items",
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    quantity = models.IntegerField(default=1)
    is_ordered = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.quantity} of {self.product.name}'


class DiscountCoupon(models.Model):
    coupon_code = models.CharField(max_length=40, unique=True)
    discount_amount = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.coupon_code} ~ {self.discount_amount} "


class Order(models.Model):
    user = models.ForeignKey(
        User,
        related_name="orders",
        on_delete=models.CASCADE
    )
    ordered_date = models.DateTimeField(auto_now_add=True)
    total = models.IntegerField(default=0)
    discount = models.IntegerField(default=0)

    class Meta:
        ordering = ['-ordered_date', ]

    def __str__(self):
        return f"{self.user} on {self.ordered_date}"
