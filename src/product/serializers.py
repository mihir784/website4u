from rest_framework import serializers
from .models import CartItem, DiscountCoupon, Order, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'description',
            'price',
            'image',
        ]


class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = [
            'id',
            'user',
            'shop',
            'product',
            'quantity',
        ]


class DiscountCouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountCoupon
        fields = [
            'id',
            'coupon_code',
            'discount_amount',
        ]


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'id',
            'total',
            'discount',
            'user',
            'ordered_date',
        ]
