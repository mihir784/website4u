from django.urls import path, include
from rest_framework import routers
from .views import (
    DiscountCouponAPIView,
    OrderHistoryView,
    ProductAPIView,
    CartAPIView,
    PaymentView
)


router = routers.DefaultRouter()
router.register('product', ProductAPIView)

cartRouter = routers.DefaultRouter()
cartRouter.register('cart', CartAPIView)

discountRouter = routers.DefaultRouter()
discountRouter.register('discount', DiscountCouponAPIView)

orderRouter = routers.DefaultRouter()
orderRouter.register('orders', OrderHistoryView)

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/', include(cartRouter.urls)),
    path('api/', include(discountRouter.urls)),
    path('api/', include(orderRouter.urls)),
    path('api/checkout/', PaymentView.as_view(), name='checkout'),
]
