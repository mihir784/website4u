from rest_framework import status
from django.http.response import HttpResponseBadRequest
from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response
from .serializers import (
    ShopSerializer,
    UserSerializer,
    RegisterSerializer,
    LoginSerializer
)
from knox.models import AuthToken
from .models import Shop


class RegisterAPIView(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        try:
            data['shop'] = Shop.objects.get(shop_name=data['shop_name']).pk
        except Exception:
            data['shop'] = None
        print(data)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(
                user,
                context=self.get_serializer_context()
            ).data,
            "token": AuthToken.objects.create(user)[1]
        })


class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        try:
            data['shop'] = Shop.objects.get(shop_name=data['shop_name']).pk
        except Exception:
            data['shop'] = None
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        userInstance = serializer.validated_data
        user = UserSerializer(
            userInstance,
            context=self.get_serializer_context()).data
        if user['shop'] == data['shop']:
            _, token = AuthToken.objects.create(userInstance)
            return Response({
                "user": user,
                "token": token
            })
        return Response({
            "non_field_errors": [
                "You are not registered on this site"
            ]
        }, status=status.HTTP_400_BAD_REQUEST)


class UserAPIView(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class ShopAPIView(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer

    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        return self.request.user.shops.all()

    def create(self, request, *args, **kwargs):
        data = request.data
        data['shop_owner'] = self.request.user.id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class ShopDetailView(generics.RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        shop_name = kwargs.get('shop_name')

        try:
            shop_obj = Shop.objects.get(shop_name=shop_name)
            print(shop_obj)
            return Response({
                "shop": ShopSerializer(
                    shop_obj,
                    context=self.get_serializer_context()
                ).data,
            })
        except Exception:
            pass

        return HttpResponseBadRequest
