from django.urls import path, include
from knox import views as knox_views
from .views import (
    RegisterAPIView,
    LoginAPIView,
    UserAPIView,
    ShopAPIView,
    ShopDetailView
)
from rest_framework import routers

router = routers.DefaultRouter()
router.register('shop', ShopAPIView)


urlpatterns = [
    path('api/auth', include('knox.urls')),
    path('api/auth/register', RegisterAPIView.as_view()),
    path('api/auth/login', LoginAPIView.as_view()),
    path('api/auth/user', UserAPIView.as_view()),
    path('api/auth/logout', knox_views.LogoutView.as_view()),
    path('api/', include(router.urls)),
    path('api/shop/<str:shop_name>', ShopDetailView.as_view()),
]
