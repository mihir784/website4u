import React, { Component, Fragment } from "react";

export class Alerts extends Component {
  componentDidUpdate(prevProps) {
    const { error, alert, message } = this.props;
    if (error !== prevProps.error) {
      if (error.msg.shop_name) {
        alert.error(`Error: ${error.msg.shop_name}`);
      }
      if (error.msg.non_field_errors) {
        alert.error(error.msg.non_field_errors.join());
      }
      if (error.msg.username) {
        alert.error(error.msg.username.join());
      }
      if (error.msg.email) {
        alert.error(error.msg.email.join());
      }
    }

    if (message !== prevProps.message) {
      if (message.deleteShop) alert.success(message.deleteShop);
      if (message.addShop) alert.success(message.addShop);
      if (message.field) alert.error(message.field);
      if (message.orderSuccessful) alert.success(message.orderSuccessful);
    }
  }

  render() {
    return <Fragment />;
  }
}

export default Alerts;
