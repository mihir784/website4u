import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const CustomerRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (auth.isLoading) {
        return <h2>Loading Customer...</h2>;
      } else if (!auth.isAuthenticated) {
        return (
          <Redirect
            to={{ pathname: `/shop/${props.match.params.shop_name}/login` }}
          />
        );
      } else if (auth.user.is_shop_owner) {
        return <Redirect to={`/shop/${props.match.params.shop_name}/lost`} />;
      } else {
        return <Component {...props} />;
      }
    }}
  />
);

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(CustomerRoute);
