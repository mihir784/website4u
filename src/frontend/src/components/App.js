import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import PrivateRoute from "./common/PrivateRoute";
import CustomerRoute from "./common/CustomerRoute";
import { Provider } from "react-redux";
import Register from "./authentication/Register";
import Login from "./authentication/Login";
import Shop from "./shop/Shop";
import CustomerLogin from "./authentication/CustomerLogin";
import CustomerRegistration from "./authentication/CustomerRegistration";
import OwnerWarning from "./authentication/OwnerWarning";
import store from "../store";
import { loadUser } from "../actions/auth";
import ShopHomepage from "./shop/ShopHomepage";
import ProductDetail from "./shop/ProductDetail";
import Cart from "./shop/Cart";
import Checkout from "./shop/Checkout";

import axios from "axios";
import OrderHistory from "./shop/OrderHistory";
import Homepage from "./shop/Homepage";

axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFToken";

// alert options
const alertOptions = {
  timeout: 3000,
  postion: "top center",
};

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <Router>
            <Fragment>
              <Switch>
                <PrivateRoute exact path="/" component={Shop} />
                <Route exact path="/home" component={Homepage} />
                <Route exact path="/shop/:shop_name" component={ShopHomepage} />
                <Route
                  exact
                  path="/shop/:shop_name/login"
                  component={CustomerLogin}
                />
                <Route
                  exact
                  path="/shop/:shop_name/register"
                  component={CustomerRegistration}
                />
                <Route
                  exact
                  path="/shop/:shop_name/lost"
                  component={OwnerWarning}
                />
                <CustomerRoute
                  exact
                  path="/shop/:shop_name/product/:product_id"
                  component={ProductDetail}
                />
                <CustomerRoute
                  exact
                  path="/shop/:shop_name/cart"
                  component={Cart}
                />
                <CustomerRoute
                  exact
                  path="/shop/:shop_name/checkout"
                  component={Checkout}
                />
                <CustomerRoute
                  exact
                  path="/shop/:shop_name/orders"
                  component={OrderHistory}
                />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
              </Switch>
            </Fragment>
          </Router>
        </AlertProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
