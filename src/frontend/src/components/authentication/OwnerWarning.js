import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/auth";
import "../../../static/frontend/index.css";

function OwnerWarning(props) {
  return (
    <div>
      <div className="company-logo">
        <p>{props.match.params.shop_name}</p>
      </div>
      <div className="AlignCenter">
        <div className="sec1">
          <p>
            You are logged in as a Shop-Owner.
            <br />
            <Link to="/">Go back to homepage</Link>
          </p>
          <p>If you want visit the store:</p>
          <p>
            {" "}
            and have a customer account{" "}
            <Link
              onClick={props.logoutUser}
              to={{
                pathname: `/shop/${props.match.params.shop_name}/login`,
              }}
            >
              login here
            </Link>
          </p>
          <p>
            Or{" "}
            <Link
              onClick={props.logoutUser}
              to={{
                pathname: `/shop/${props.match.params.shop_name}/register`,
              }}
            >
              register here
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { logoutUser })(OwnerWarning);
