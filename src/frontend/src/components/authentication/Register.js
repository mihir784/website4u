import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { registerUser } from "../../actions/auth";
import "../../../static/frontend/index.css";
import { withAlert } from "react-alert";
import Alerts from "../Alerts";

export class Register extends Component {
  state = {
    username: "",
    email: "",
    password: "",
  };

  static propTypes = {
    registerUser: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    errors: PropTypes.object.isRequired,
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.registerUser(
      this.state.username,
      this.state.email,
      this.state.password,
      true
    );
    this.setState({
      username: "",
      email: "",
      password: "",
    });
  };

  onChange = (e) =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    const { username, email, password } = this.state;
    return (
      <div>
        <Alerts error={this.props.errors} alert={this.props.alert} />
        <div className="background-image">
          <div className="login-form">
            <Link to={{ pathname: `/home` }}>
              <div className="home-logo">
                <img
                  src="https://commandc.com/wp-content/uploads/2016/03/shopify-bag.png"
                  alt=""
                />
                <p>Website4u</p>
              </div>
            </Link>
            <div className="form-heading">
              <p>Register</p>
              <p className="color-grey">Continue with website4u</p>
            </div>
            <form onSubmit={this.onSubmit} className="form-styling">
              <input
                type="username"
                name="username"
                placeholder="Username"
                onChange={this.onChange}
                value={username}
              />
              <input
                type="email"
                name="email"
                placeholder="Email"
                onChange={this.onChange}
                value={email}
              />
              <input
                type="password"
                name="password"
                placeholder="Password"
                onChange={this.onChange}
                value={password}
              />

              <button type="submit">REGISTER</button>
              <p className="form-register">
                Already Have an account?
                <Link className="color-green" to="/login">
                  {" "}
                  Login{" "}
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  errors: state.errors,
});

export default connect(mapStateToProps, { registerUser })(
  withAlert()(Register)
);
