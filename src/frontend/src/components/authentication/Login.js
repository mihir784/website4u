import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loginUser } from "../../actions/auth";
import "../../../static/frontend/index.css";
import Alerts from "../Alerts";
import { withAlert } from "react-alert";

export class Login extends Component {
  state = {
    username: "",
    password: "",
  };

  static propTypes = {
    loginUser: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    errors: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.loginUser(this.state.username, this.state.password);
  };

  onChange = (e) =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    const { username, password } = this.state;
    return (
      <div>
        <Alerts
          error={this.props.errors}
          alert={this.props.alert}
          message={this.props.message}
        />
        <div className="background-image">
          <div className="login-form">
            <Link to={{ pathname: `/home` }}>
              <div className="home-logo">
                <img
                  src="https://commandc.com/wp-content/uploads/2016/03/shopify-bag.png"
                  alt=""
                />
                <p>Website4u</p>
              </div>
            </Link>
            <div className="form-heading">
              <p>Login</p>
              <p className="color-grey">Continue with website4u</p>
            </div>
            <form onSubmit={this.onSubmit} className="form-styling">
              <input
                type="username"
                name="username"
                placeholder="username"
                onChange={this.onChange}
                value={username}
              />
              <input
                type="password"
                name="password"
                placeholder="password"
                onChange={this.onChange}
                value={password}
              />

              <button type="submit">LOGIN</button>
              <p className="form-register">
                New to website4u?
                <Link className="color-green" to="/register">
                  {" "}
                  Get Started{" "}
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  errors: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps, { loginUser })(withAlert()(Login));
