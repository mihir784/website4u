import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { registerCustomer } from "../../actions/auth";
import "../../../static/frontend/index.css";
import Alerts from "../Alerts";
import { withAlert } from "react-alert";

function CustomerRegistration(props) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const shop_name = props.match.params.shop_name;
  const onSubmit = (e) => {
    e.preventDefault();
    props.registerCustomer(username, email, password, shop_name);
  };

  if (props.auth.isAuthenticated && !props.auth.user.is_shop_owner) {
    return <Redirect to={{ pathname: `/shop/${shop_name}` }} />;
  }
  return (
    <div>
      <Alerts
        error={props.errors}
        alert={props.alert}
        message={props.message}
      />
      <div className="background-image-blue">
        <div className="login-form">
          <Link to={{ pathname: `/home` }}>
            <div className="home-logo">
              <p>Shop {shop_name}</p>
            </div>
          </Link>
          <div className="form-heading">
            <p>Register</p>
          </div>
          <form onSubmit={onSubmit} className="form-styling">
            <input
              type="username"
              placeholder="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <input
              type="email"
              placeholder="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />

            <button id="bg-blue" type="submit">
              REGISTER
            </button>
            <p className="form-register">
              Already Have an account?
              <Link
                className="color-blue"
                to={{ pathname: `/shop/${shop_name}/login` }}
              >
                {" "}
                Login{" "}
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps, { registerCustomer })(
  withAlert()(CustomerRegistration)
);
