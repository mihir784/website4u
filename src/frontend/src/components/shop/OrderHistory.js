import axios from "axios";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { loadCart, loadProducts, loadShop } from "../../actions/shop";
import store from "../../store";
import { DISCOUNT_RESET } from "../../actions/types";

export class OrderHistory extends Component {
  state = {
    orderHistory: [],
    error: false,
  };
  static propTypes = {
    user: PropTypes.object.isRequired,
    shop: PropTypes.object.isRequired,
  };
  componentDidMount() {
    const shop_name = this.props.match.params.shop_name;
    store.dispatch(loadShop(shop_name));

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = `Token ${token}`;
    }

    axios
      .get("/api/orders/", config)
      .then((res) => {
        this.setState({ orderHistory: res.data, error: false });
      })
      .catch((err) => {
        console.log(res.err);
        this.setState({ orderHistory: [], error: true });
      });

    store.dispatch(loadProducts());
    store.dispatch(loadCart());
    store.dispatch({ type: DISCOUNT_RESET });
  }

  render() {
    const { shop_name, shop_image } = this.props.shop;
    console.log(this.state.orderHistory);
    return (
      <div>
        <header>
          <nav className="navbar navbar-expand-lg bg-blue">
            <div className="container-fluid">
              <Link
                className="navbar-brand"
                to={{ pathname: `/shop/${shop_name}` }}
              >
                <img
                  src={shop_image}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                  alt=""
                />
                <div className="d-inline-block margin-left color-white">
                  {shop_name}
                </div>
              </Link>
              <div className="icons">
                <Link to={{ pathname: `/shop/${shop_name}/cart` }}>
                  <i
                    className="fa fa-shopping-cart color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
                <Link to={{ pathname: `/shop/${shop_name}/orders` }}>
                  <i
                    className="fa fa-history color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
              </div>
            </div>
          </nav>
        </header>
        <div className="center margin-bottom">
          <h2>Order History</h2>
        </div>
        {this.state.orderHistory.length == 0 && (
          <div className="center mt-5">No orders till Now</div>
        )}
        <div className="table-center">
          <table className="table table-striped">
            <tbody>
              <th className="big-text">OrderDate</th>
              <th className="big-text">Sub-Total</th>
              <th className="big-text">Discount</th>
              <th className="big-text">Total</th>
              {this.state.orderHistory.map((order) => (
                <tr key={order.id}>
                  <td className="adjust">
                    <p className="bold">{order.ordered_date.slice(2, 10)}</p>
                  </td>
                  <td>
                    <p className="bold">{order.total}</p>
                  </td>
                  <td>
                    <p className="small">Rs. {order.discount}</p>
                  </td>
                  <td>
                    <p className="bold">Rs. {order.total}</p>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.auth.user,
  shop: state.shopData.shop,
});

export default connect(mapStateToProps, { loadShop })(OrderHistory);
