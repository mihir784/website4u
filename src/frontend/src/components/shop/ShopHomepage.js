import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import store from "../../store";
import PropTypes from "prop-types";
import { loadShop, loadProducts } from "../../actions/shop";
import { logoutUser } from "../../actions/auth";
import auth from "../../reducers/auth";

export class ShopHomepage extends Component {
  static propTypes = {
    shop: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    loadProducts: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const shop_name = this.props.match.params.shop_name;
    store.dispatch(loadShop(shop_name));
    store.dispatch(loadProducts());
  }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.match.params.shop_name || "") !=
      this.props.match.params.shop_name
    ) {
      const shop_name = this.props.match.params.shop_name;
      store.dispatch(loadShop(shop_name));
    }
  }

  render() {
    const { shop_name, shop_image, shop_owner } = this.props.shop;
    const products = this.props.products;
    if (shop_name) {
      return (
        <>
          <header>
            <nav className="navbar navbar-expand-lg bg-blue">
              <div className="container-fluid">
                <Link
                  className="navbar-brand"
                  to={{ pathname: `/shop/${shop_name}` }}
                >
                  <img
                    src={shop_image}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt=""
                  />
                  <div className="d-inline-block margin-left color-white">
                    {shop_name}
                  </div>
                </Link>
                <div className="icons">
                  <Link to={{ pathname: `/shop/${shop_name}/cart` }}>
                    <i
                      className="fa fa-shopping-cart color-white"
                      aria-hidden="true"
                    ></i>
                  </Link>
                  <Link to={{ pathname: `/shop/${shop_name}/orders` }}>
                    <i
                      className="fa fa-history color-white"
                      aria-hidden="true"
                    ></i>
                  </Link>
                  {this.props.auth.isAuthenticated &&
                    !this.props.auth.user.is_shop_owner && (
                      <Link
                        className="color-white"
                        onClick={this.props.logoutUser}
                        to={`/shop/${shop_name}/`}
                      >
                        <i className="fa fa-sign-out" aria-hidden="true"></i>
                      </Link>
                    )}
                  {!this.props.auth.isAuthenticated && (
                    <Link
                      className="color-white"
                      onClick={this.props.logoutUser}
                      to={`/shop/${shop_name}/login`}
                    >
                      <i className="fa fa-sign-in" aria-hidden="true"></i>
                    </Link>
                  )}
                </div>
              </div>
            </nav>
          </header>
          <div className="products-heading">
            <h2>{shop_name}</h2>
          </div>
          <section className="products-display">
            {products.map((product) => (
              <div className="product" key={product.id}>
                <Link
                  to={{
                    pathname: `/shop/${shop_name}/product/${product.id}`,
                  }}
                >
                  <div className="product-image">
                    <img src={product.image} alt="" />
                  </div>
                </Link>
                <div className="product-extra-info">
                  <p className="home-product-name">{product.name}</p>
                  <p className="home-product-price">{product.price}</p>
                </div>
              </div>
            ))}
          </section>
        </>
      );
    }
    return <div>No Such Shop</div>;
  }
}

const mapStateToProps = (state) => ({
  shop: state.shopData.shop,
  products: state.shopData.products,
  auth: state.auth,
});

export default connect(mapStateToProps, { logoutUser, loadProducts })(
  ShopHomepage
);
