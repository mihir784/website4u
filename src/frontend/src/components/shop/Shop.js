import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logoutUser } from "../../actions/auth";
import { getShops, deleteShop, addShop } from "../../actions/shop";
import { Form } from "./Form";
import { Alerts } from "../Alerts";
import { withAlert } from "react-alert";
import messages from "../../reducers/messages";
import auth from "../../reducers/auth";

export class Shop extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
    shops: PropTypes.array.isRequired,
    getShops: PropTypes.func.isRequired,
    deleteShop: PropTypes.func.isRequired,
    addShop: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.getShops();
  }

  render() {
    return (
      <div>
        <Fragment>
          <Alerts
            error={this.props.errors}
            alert={this.props.alert}
            message={this.props.message}
          />

          <div className="background-image">
            <div className="shop-section">
              <div className="section-shop">
                <Link to={{ pathname: `/home` }}>
                  <div className="shop-logo">
                    <img
                      src="https://commandc.com/wp-content/uploads/2016/03/shopify-bag.png"
                      alt=""
                    />
                    <p>Website4u</p>
                  </div>
                </Link>

                <Link
                  className="logout-link color-black"
                  onClick={this.props.logoutUser}
                  to={{ pathname: `/` }}
                >
                  <a href="">Logout</a>
                </Link>
              </div>

              <Form
                parentMethods={this.props.addShop}
                user={this.props.auth.user.id}
              />
              <div className="shop-list">
                <p>Your Shops</p>

                <div className="display-shop">
                  {this.props.shops.map((shop) => (
                    <div className="single-shop">
                      <div className="single-shop-image">
                        <img
                          src={shop.shop_image}
                          alt=""
                          width="40px"
                          height="40px"
                        />
                      </div>
                      <div className="single-shop-name">{shop.shop_name}</div>
                      <Link
                        className="border-none"
                        to={`/shop/${shop.shop_name}`}
                        style={{
                          textDecoration: "none",
                          color: "white",
                        }}
                      >
                        <button>
                          <i className="fa fa-eye"></i>
                        </button>
                      </Link>
                      <button
                        onClick={this.props.deleteShop.bind(this, shop.id)}
                      >
                        <i className="fa fa-times"></i>
                      </button>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  shops: state.shopData.shops,
  errors: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps, {
  logoutUser,
  getShops,
  deleteShop,
  addShop,
})(withAlert()(Shop));
