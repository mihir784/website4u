import React, { Component } from "react";
import { Alerts } from "../Alerts";

export class Form extends Component {
  state = {
    shop_name: "",
    shop_image: null,
  };

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onImageChange = (e) => {
    this.setState({
      shop_image: e.target.files[0],
    });
  };

  render() {
    return (
      <div>
        <Alerts />
        <div className="shop-form">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              this.props.parentMethods.bind(this)(
                this.state.shop_name,
                this.state.shop_image,
                this.props.user
              );
              this.setState({
                shop_name: "",
              });
            }}
            className="form-styling form-width"
          >
            <p className="big-text">Add Shop</p>
            <input
              type="text"
              name="shop_name"
              placeholder="Shop Name"
              onChange={this.onChange}
              value={this.state.shop_name}
            />
            <input
              id="image-add-button"
              type="file"
              name="shop_image"
              placeholder="Image"
              onChange={this.onImageChange}
              required
            />
            <button type="submit">Add</button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default Form;
