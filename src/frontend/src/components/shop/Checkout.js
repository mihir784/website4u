import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  loadCart,
  loadProducts,
  loadShop,
  applyCouponCode,
} from "../../actions/shop";
import store from "../../store";
import { Link } from "react-router-dom";
import {
  CardElement,
  injectStripe,
  Elements,
  StripeProvider,
} from "react-stripe-elements";
import axios from "axios";
import { createMessage, returnErrors } from "../../actions/messages";
import Alerts from "../Alerts";
import { withAlert } from "react-alert";
import { DISCOUNT_RESET } from "../../actions/types";

class CheckoutForm extends Component {
  state = {
    loading: false,
    error: null,
    success: false,
  };

  submit = (e) => {
    e.preventDefault();
    for (var value in this.props.mainState) {
      if (
        this.props.mainState[value] == "" &&
        value != "" &&
        value != "couponCode"
      ) {
        store.dispatch(
          returnErrors({ non_field_errors: [`${value} cannot be empty`] }, 400)
        );
        return;
      }
    }
    this.setState({ loading: true });
    if (this.props.stripe) {
      this.props.stripe.createToken().then((result) => {
        if (result.error) {
          this.setState({ error: result.error.message, loading: false });
        } else {
          const config = {
            headers: {
              "Content-Type": "application/json",
            },
          };

          const token = localStorage.getItem("token");
          if (token) {
            config.headers["Authorization"] = `Token ${token}`;
          }

          axios
            .post(
              "/api/checkout/",
              {
                stripeToken: result.token.id,
                discount: this.props.discountAmount,
                email: this.props.mainState.email,
                phoneNumber: this.props.mainState.phoneNumber,
                address: this.props.mainState.address,
                city: this.props.mainState.city,
                state: this.props.mainState.state,
                zipCode: this.props.mainState.zipCode,
                country: this.props.mainState.country,
              },
              config
            )
            .then((res) => {
              this.setState({ loading: false, success: true, error: null });
              store.dispatch(
                createMessage({
                  orderSuccessful: "Order Completed Successfully",
                })
              );
              store.dispatch({ type: DISCOUNT_RESET });
              this.props.redirect();
            })
            .catch((err) => {
              console.log(err);
              this.setState({ loading: false, success: false, error: err });
            });
        }
      });
    } else {
      console.log("Stripe is not loaded");
    }
  };

  render() {
    const { error, loading, success, ...rest } = this.state;
    return (
      <div>
        {error && (
          <div className="payment-message-danger">
            <p>Your payment was unsuccessful</p>
          </div>
        )}
        {success && (
          <div className="payment-message-success">
            <p>Your payment was successful</p>
          </div>
        )}
        <CardElement />
        <button
          loading={loading.toString()}
          disabled={loading}
          onClick={this.submit}
          className="place-order-button"
          style={{ marginTop: "30px" }}
        >
          Place Order
        </button>
      </div>
    );
  }
}

const InjectedForm = injectStripe(CheckoutForm);

export class Checkout extends Component {
  state = {
    email: "",
    phoneNumber: "",
    address: "",
    city: "",
    state: "",
    zipCode: "",
    country: "",
    couponCode: "",
  };
  static propTypes = {
    shop: PropTypes.object.isRequired,
    orders: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
    discountAmount: PropTypes.number.isRequired,
    applyCouponCode: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
  };

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  applyCoupon = (e) => {
    e.preventDefault();
    this.props.applyCouponCode(this.state.couponCode);
    this.setState({
      [e.target.name]: "",
    });
  };

  redirect = (e) => {
    this.props.history.push(`/shop/${this.props.shop.shop_name}/orders`);
  };

  componentDidMount() {
    const shop_name = this.props.match.params.shop_name;
    store.dispatch(loadShop(shop_name));
    store.dispatch(loadProducts());
    store.dispatch(loadCart());
  }

  render() {
    const { shop_name, shop_image } = this.props.shop;
    return (
      <>
        <Alerts
          error={this.props.errors}
          alert={this.props.alert}
          message={this.props.message}
        />
        <header>
          <nav className="navbar navbar-expand-lg bg-blue">
            <div className="container-fluid">
              <Link
                className="navbar-brand"
                to={{ pathname: `/shop/${shop_name}` }}
              >
                <img
                  src={shop_image}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                  alt=""
                />
                <div className="d-inline-block margin-left color-white">
                  {shop_name}
                </div>
              </Link>
              <div className="icons">
                <Link to={{ pathname: `/shop/${shop_name}/cart` }}>
                  <i
                    className="fa fa-shopping-cart color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
                <Link to={{ pathname: `/shop/${shop_name}/orders` }}>
                  <i
                    className="fa fa-history color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
              </div>
            </div>
          </nav>
        </header>
        <section className="product-info">
          <p className="big-text">Checkout</p>
        </section>
        <StripeProvider
          apiKey="pk_test_51JklYYSC0dMKlMeiGpDepgpp25nu1dq6RbMM2TXaDYH2SquD8cfZ6myqn93Eminy0oXjeuGnYJHOa9nfyT8X421T00Opv5wOHk"
          id="pay"
          className="text-fields"
        >
          <div className="form-section">
            <form
              className="checkout-form"
              onSubmit={this.onClick}
              method="POST"
            >
              <section className="section-1">
                <div id="account-info" className="text-field">
                  <p className="heading">Account Information</p>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    value={this.state.email}
                    onChange={this.onChange}
                    placeholder="Email"
                  />
                </div>
                <div id="phone" className="text-field">
                  <input
                    type="tel"
                    name="phoneNumber"
                    id="phone-number"
                    value={this.state.phoneNumber}
                    onChange={this.onChange}
                    placeholder="Phone Number"
                  />
                </div>
                <div id="shipping" className="text-field">
                  <p className="heading">Shipping Address</p>
                  <input
                    type="text"
                    name="address"
                    id="address"
                    value={this.state.address}
                    onChange={this.onChange}
                    placeholder="Address"
                  />
                </div>
                <div id="city" className="text-field">
                  <input
                    type="text"
                    name="city"
                    id="city"
                    placeholder="City"
                    value={this.state.city}
                    onChange={this.onChange}
                  />
                </div>
                <div id="state" className="text-field">
                  <input
                    type="text"
                    name="state"
                    id="state-province"
                    value={this.state.state}
                    onChange={this.onChange}
                    placeholder="State/Province"
                  />
                </div>
                <div id="zip" className="text-field">
                  <input
                    type="number"
                    name="zipCode"
                    id="zipcode"
                    value={this.state.zipCode}
                    onChange={this.onChange}
                    placeholder="zipcode"
                  />
                </div>
                <div id="countries" className="text-field">
                  <label htmlFor="country">Country</label>
                  <input
                    type="text"
                    name="country"
                    id="country"
                    value={this.state.country}
                    onChange={this.onChange}
                    placeholder="Country"
                  />
                </div>
              </section>

              <section className="summary">
                <p className="heading">Order Summary</p>
                <div className="receipt">
                  {this.props.orders.map((order) => (
                    <div className="pro1" key={order.id}>
                      <span className="product-name">{order.name}</span>
                      <span className="product-price">
                        RS {order.price} x {order.quantity}
                      </span>
                    </div>
                  ))}
                  {this.props.discountAmount == 0 ? (
                    <div
                      style={{
                        margin: "12px 0",
                        width: "100%",
                        display: "flex",
                        paddingRight: "12px",
                      }}
                    >
                      <input
                        style={{
                          display: "inline-block",
                          width: "auto",
                          flex: "1 0",
                        }}
                        type="text"
                        name="couponCode"
                        id="coupon-code"
                        value={this.state.couponCode}
                        onChange={this.onChange}
                        placeholder="Have a Coupon Code?"
                      />
                      <button
                        className="btn btn-primary btn-sm"
                        style={{
                          margin: "0 10px",
                        }}
                        type="submit"
                        onClick={this.applyCoupon}
                      >
                        Apply
                      </button>
                    </div>
                  ) : (
                    <div
                      style={{
                        margin: "12px 0",
                        textAlign: "right",
                        width: "100%",
                      }}
                    >
                      {this.state.couponCode} Applied, Discount:{" "}
                      {this.props.discountAmount}
                    </div>
                  )}
                  <div className="total">
                    <span className="total-tag">Total</span>
                    <span className="product-price">RS {this.props.total}</span>
                  </div>
                </div>
              </section>

              <section className="summary">
                <div>
                  <p className="heading">Card Details</p>
                  <Elements>
                    <InjectedForm
                      mainState={this.state}
                      discountAmount={this.props.discountAmount}
                      shop_name={shop_name}
                      redirect={this.redirect}
                    />
                  </Elements>
                </div>
              </section>
            </form>
          </div>
        </StripeProvider>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  shop: state.shopData.shop,
  orders: state.shopData.orders,
  total: state.shopData.total,
  discountAmount: state.shopData.discountAmount,
  errors: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps, { loadShop, applyCouponCode })(
  withAlert()(Checkout)
);
