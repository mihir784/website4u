import React, { Component } from "react";
import { Link } from "react-router-dom";
import { logoutUser } from "../../actions/auth";
import auth from "../../reducers/auth";
import store from "../../store";
import PropTypes from "prop-types";
import { connect } from "react-redux";

export class Homepage extends Component {
  static propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div>
        <header className="home-header">
          <Link to={{ pathname: `/home` }}>
            <div className="home-logo">
              <img
                src="https://commandc.com/wp-content/uploads/2016/03/shopify-bag.png"
                alt=""
              />
              <p>Website4u</p>
            </div>
          </Link>

          {this.props.auth.isAuthenticated && (
            <div className="login-link">
              <Link
                className="color-black"
                onClick={this.props.logoutUser}
                to={{ pathname: `/login` }}
              >
                <a href="">Logout</a>
              </Link>
            </div>
          )}
          {!this.props.auth.isAuthenticated && (
            <div className="login-link">
              <Link
                className="color-black"
                onclick={this.props.logoutUser}
                to={{ pathname: `/login` }}
              >
                <a href="">Login</a>
              </Link>
            </div>
          )}
        </header>

        <section className="home-content">
          <div className="quote">
            <p>Anyone, anywhere can start an ecommerce website</p>
          </div>
          <div className="quote-image">
            <img src="../../../media/1.png" alt="" />
          </div>
        </section>
        <footer className="home-footer"></footer>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  products: state.shopData.products,
  auth: state.auth,
});

export default connect(mapStateToProps, { logoutUser })(Homepage);
