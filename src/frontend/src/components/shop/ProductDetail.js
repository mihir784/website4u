import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import {
  loadProducts,
  addToCart,
  loadShop,
  loadCart,
} from "../../actions/shop";
import store from "../../store";

export class ProductDetail extends Component {
  state = {
    quantity: 1,
    product_id: null,
  };
  static propTypes = {
    shop: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    cartItems: PropTypes.array.isRequired,
  };

  componentDidMount() {
    store.dispatch(loadProducts());
    const shop_name = this.props.match.params.shop_name;
    store.dispatch(loadShop(shop_name));
    store.dispatch(loadCart());
  }

  render() {
    const { shop_name, shop_image } = this.props.shop;
    const product = this.props.products.find(
      (product) => product.id == this.props.match.params.product_id
    );
    var added = false;
    try {
      const cartItem = this.props.cartItems.find(
        (cartItem) =>
          cartItem.user == this.props.user.id &&
          cartItem.product == +this.props.match.params.product_id &&
          cartItem.shop == this.props.user.shop
      );
      if (cartItem) added = true;
    } catch (e) {}

    if (product) {
      return (
        <>
          <header>
            <nav className="navbar navbar-expand-lg bg-blue">
              <div className="container-fluid">
                <Link
                  className="navbar-brand"
                  to={{ pathname: `/shop/${shop_name}` }}
                >
                  <img
                    src={shop_image}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt=""
                  />
                  <div className="d-inline-block margin-left color-white">
                    {shop_name}
                  </div>
                </Link>
                <div className="icons">
                  <Link to={{ pathname: `/shop/${shop_name}/cart` }}>
                    <i
                      className="fa fa-shopping-cart color-white"
                      aria-hidden="true"
                    ></i>
                  </Link>
                  <Link to={{ pathname: `/shop/${shop_name}/orders` }}>
                    <i
                      className="fa fa-history color-white"
                      aria-hidden="true"
                    ></i>
                  </Link>
                </div>
              </div>
            </nav>
          </header>
          <section className="section-product1">
            <div className="big-image1">
              <img src={product.image} alt="" width="350px" height="350px" />
            </div>
            <div className="image-info1">
              <div className="text1">
                <p className="product-name1">{product.name}</p>
                <p className="product-price1">{product.price}</p>
              </div>
            </div>

            <div className="description1">
              <p className="big-description1">Description</p>
              <p className="brief1">{product.description}</p>
            </div>

            <form className="cart-form">
              {!added && <label htmlFor="quantity">Quantity</label>}
              {!added && (
                <input
                  type="number"
                  name="quantity"
                  value={this.state.quantity}
                  style={{ background: "#e3e3e3", textAlign: "center" }}
                  onChange={(e) =>
                    this.setState({ [e.target.name]: e.target.value })
                  }
                  placeholder="1"
                />
              )}
              {added && <div>Item Added to Cart</div>}
            </form>
          </section>

          {!added && (
            <button
              className="add-to-cart1"
              type="button"
              onClick={() =>
                this.props.addToCart(
                  this.props.user.id,
                  this.props.match.params.product_id,
                  this.props.user.shop,
                  this.state.quantity
                )
              }
            >
              {" "}
              ADD TO CART{" "}
            </button>
          )}
          {added && (
            <Link
              to={{
                pathname: `/shop/${this.props.match.params.shop_name}/cart`,
              }}
            >
              <button className="add-to-cart1">
                {" "}
                <p> VIEW CART </p>
              </button>
            </Link>
          )}
        </>
      );
    }
    return <div>Loading Product...</div>;
  }
}

const mapStateToProps = (state) => ({
  shop: state.shopData.shop,
  products: state.shopData.products,
  cartItems: state.shopData.cartItems,
  user: state.auth.user,
});

export default connect(mapStateToProps, { loadProducts, addToCart, loadShop })(
  ProductDetail
);
