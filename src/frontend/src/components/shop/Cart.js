import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import store from "../../store";
import PropTypes from "prop-types";
import { loadShop, loadProducts, loadCart } from "../../actions/shop";
import { DISCOUNT_RESET } from "../../actions/types";

export class Cart extends Component {
  static propTypes = {
    shop: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
    cartItems: PropTypes.array.isRequired,
    loadProducts: PropTypes.func.isRequired,
    orders: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
  };

  componentDidMount() {
    const shop_name = this.props.match.params.shop_name;
    store.dispatch(loadShop(shop_name));
    store.dispatch(loadProducts());
    store.dispatch(loadCart());
  }

  render() {
    const { shop_name, shop_image } = this.props.shop;
    return (
      <>
        <header>
          <nav className="navbar navbar-expand-lg bg-blue">
            <div className="container-fluid">
              <Link
                className="navbar-brand"
                to={{ pathname: `/shop/${shop_name}` }}
              >
                <img
                  src={shop_image}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                  alt=""
                />
                <div className="d-inline-block margin-left color-white">
                  {shop_name}
                </div>
              </Link>
              <div className="icons">
                <Link to={{ pathname: `/shop/${shop_name}/cart` }}>
                  <i
                    className="fa fa-shopping-cart color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
                <Link to={{ pathname: `/shop/${shop_name}/orders` }}>
                  <i
                    className="fa fa-history color-white"
                    aria-hidden="true"
                  ></i>
                </Link>
              </div>
            </div>
          </nav>
        </header>
        <section className="product-info">
          <p className="big-text">Your Cart</p>
          <p className="small-text">({this.props.orders.length} items)</p>
        </section>
        {this.props.orders.length > 0 ? (
          <div className="table-center">
            <table className="table table-striped">
              <tbody>
                <th colspan="2" className="big-text center">
                  Product
                </th>
                <th className="big-text">Price</th>
                <th className="big-text">Quantity</th>
                <th className="big-text">Total</th>
                {this.props.orders.map((orderItem) => (
                  <tr key={orderItem.id}>
                    <td className="adjust">
                      <img
                        style={{ width: "100px", height: "100px" }}
                        src={orderItem.image}
                      />
                    </td>
                    <td>
                      <p className="bold">{orderItem.name}</p>
                    </td>
                    <td>
                      <p className="small">Rs. {orderItem.price}</p>
                    </td>
                    <td>
                      <p className="bold">{orderItem.quantity}</p>
                    </td>
                    <td>
                      <p className="bold">
                        Rs. {orderItem.quantity * orderItem.price}
                      </p>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="total-price">
              <span>Total: </span>
              <span>Rs. {this.props.total}</span>
              <Link
                className="navbar-brand"
                to={{ pathname: `/shop/${shop_name}/checkout` }}
              >
                <button className="checkout-button" type="button">
                  {" "}
                  <p>CHECKOUT</p>
                </button>
              </Link>
            </div>
            <Link
              className="navbar-brand"
              to={{ pathname: `/shop/${shop_name}/checkout` }}
            >
              <button className="checkout-button-mobile" type="button">
                {" "}
                <p>CHECKOUT</p>
              </button>
            </Link>
          </div>
        ) : (
          <div className="center margin-bottom">
            <h1>Cart is Empty !!</h1>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  shop: state.shopData.shop,
  products: state.shopData.products,
  cartItems: state.shopData.cartItems,
  orders: state.shopData.orders,
  total: state.shopData.total,
});

export default connect(mapStateToProps, { loadProducts, loadShop })(Cart);
