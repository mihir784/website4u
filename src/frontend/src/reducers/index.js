import { combineReducers } from "redux";
import auth from "./auth";
import shopData from "./shop";
import errors from "./errors";
import messages from "./messages";

export default combineReducers({
  auth,
  shopData,
  errors,
  messages,
});
