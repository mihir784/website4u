import {
  GET_SHOPS,
  DELETE_SHOP,
  ADD_SHOP,
  SHOP_LOADED,
  PRODUCTS_LOADED,
  CART_ITEM_ADDED,
  CART_LOADED,
  CART_DISPLAY,
  DISCOUNT_APPLIED,
  DISCOUNT_RESET,
} from "../actions/types";

const initialState = {
  shops: [],
  shop: {},
  products: [],
  cartItems: [],
  orders: [],
  total: 0,
  discountAmount: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_SHOPS:
      return {
        ...state,
        shops: action.payload,
      };
    case DELETE_SHOP:
      return {
        ...state,
        shops: state.shops.filter((shop) => shop.id !== action.payload),
      };
    case SHOP_LOADED:
      return {
        ...state,
        shop: action.payload,
      };
    case ADD_SHOP:
      return {
        ...state,
        shops: [...state.shops, action.payload],
      };
    case PRODUCTS_LOADED:
      return {
        ...state,
        products: action.payload,
      };
    case CART_ITEM_ADDED:
      return {
        ...state,
        cartItems: [...state.cartItems, action.payload],
      };
    case CART_LOADED:
      return {
        ...state,
        cartItems: action.payload,
      };
    case CART_DISPLAY:
      if (state.products.length > 0 && state.cartItems.length > 0) {
        var orders = [];
        var total = 0;
        state.cartItems.map((cartItem) => {
          var orderItem = {};
          var currentProduct = state.products.find(
            (product) => product.id == cartItem.product
          );
          orderItem["id"] = cartItem.id;
          orderItem["quantity"] = cartItem.quantity;
          orderItem["name"] = currentProduct["name"];
          orderItem["price"] = currentProduct["price"];
          orderItem["image"] = currentProduct["image"];
          orders.push(orderItem);
          total += cartItem.quantity * currentProduct.price;
        });
        return {
          ...state,
          orders: orders,
          total: parseFloat(total.toFixed(2)),
        };
      }
      return {
        ...state,
        orders: [],
        total: 0,
      };
    case DISCOUNT_APPLIED:
      return {
        ...state,
        discountAmount: action.payload,
        total: state.total - action.payload,
      };
    case DISCOUNT_RESET:
      return {
        ...state,
        discountAmount: 0,
      };
    default:
      return state;
  }
}
