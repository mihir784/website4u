import axios from "axios";
import { returnErrors } from "./messages";

import {
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  NO_ERRORS,
} from "./types";

export const loadUser = () => (dispatch, getState) => {
  axios
    .get("api/auth/user", getConfig(getState))
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: USER_LOADED,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({
        type: AUTH_ERROR,
      });
    });
};

export const loginUser = (username, password) => (dispatch) => {
  const body = JSON.stringify({
    username,
    password,
  });

  axios
    .post("api/auth/login", body, getConfig())
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({
        type: LOGIN_FAILED,
      });
    });
};

export const logoutUser = () => (dispatch, getState) => {
  axios
    .post("api/auth/logout/", null, getConfig(getState))
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: LOGOUT_SUCCESS,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const registerUser =
  (username, email, password, is_shop_owner = false) =>
  (dispatch) => {
    const body = JSON.stringify({
      username,
      email,
      password,
      is_shop_owner,
    });

    axios
      .post("api/auth/register", body, getConfig())
      .then((res) => {
        dispatch({
          type: NO_ERRORS,
        });
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data,
        });
      })
      .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
        dispatch({
          type: REGISTER_FAILED,
        });
      });
  };

export const getConfig = (getState = null) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  if (getState != null) {
    const token = getState().auth.token;
    if (token) {
      config.headers["Authorization"] = `Token ${token}`;
    }
  }
  return config;
};

export const registerCustomer =
  (username, email, password, shop_name) => (dispatch) => {
    const body = JSON.stringify({
      username,
      email,
      password,
      shop_name,
      is_shop_owner: false,
    });

    axios
      .post("api/auth/register", body, getConfig())
      .then((res) => {
        dispatch({
          type: NO_ERRORS,
        });
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data,
        });
      })
      .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
        dispatch({
          type: REGISTER_FAILED,
        });
      });
  };

export const loginCustomer = (username, password, shop_name) => (dispatch) => {
  const body = JSON.stringify({
    username,
    password,
    shop_name,
  });

  axios
    .post("api/auth/login", body, getConfig())
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({
        type: LOGIN_FAILED,
      });
    });
};
