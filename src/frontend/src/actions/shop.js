import axios from "axios";
import { createMessage, returnErrors } from "./messages";

import {
  GET_SHOPS,
  DELETE_SHOP,
  ADD_SHOP,
  SHOP_LOADED,
  NO_ERRORS,
  PRODUCTS_LOADED,
  CART_ITEM_ADDED,
  CART_LOADED,
  CART_DISPLAY,
  DISCOUNT_APPLIED,
  DISCOUNT_RESET,
} from "./types";
import { getConfig } from "./auth";

// GET
export const getShops = () => (dispatch, getState) => {
  axios
    .get("api/shop/", getConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_SHOPS,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

//ADD SHOP
export const addShop = (shop_name, shop_image) => (dispatch, getState) => {
  const config = {
    headers: {
      accept: "application/json",
      "Content-Type": "multipart/form-data",
    },
  };

  const token = getState().auth.token;
  if (token) {
    config.headers["Authorization"] = `Token ${token}`;
  }

  const formData = new FormData();
  formData.append("shop_image", shop_image);
  formData.append("shop_name", shop_name);

  axios
    .post("api/shop/", formData, config)
    .then((res) => {
      dispatch(createMessage({ addShop: "Shop Added" }));
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: ADD_SHOP,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

//DELETE
export const deleteShop = (id) => (dispatch, getState) => {
  axios
    .delete(`api/shop/${id}/`, getConfig(getState))
    .then((res) => {
      dispatch(createMessage({ deleteShop: "Shop deleted" }));
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: DELETE_SHOP,
        payload: id,
      });
    })
    .catch((err) => console.log(err, err.response));
};

export const loadShop = (shop_name) => (dispatch) => {
  axios
    .get(`api/shop/${shop_name}`)
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: SHOP_LOADED,
        payload: res.data.shop,
      });
    })
    .catch((err) => {
      console.log(err);
      dispatch({
        type: SHOP_LOADED,
        payload: {},
      });
    });
};

export const loadProducts = () => (dispatch) => {
  axios
    .get("api/product")
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: PRODUCTS_LOADED,
        payload: res.data,
      });
      dispatch({
        type: CART_DISPLAY,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("err", err);
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const addToCart =
  (user, product, shop, quantity) => (dispatch, getState) => {
    const body = JSON.stringify({
      user,
      product,
      shop,
      quantity,
    });

    axios
      .post("api/cart/", body, getConfig(getState))
      .then((res) => {
        dispatch({
          type: NO_ERRORS,
        });
        dispatch({
          type: CART_ITEM_ADDED,
          payload: res.data,
        });
      })
      .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
      });
  };

export const loadCart = () => (dispatch, getState) => {
  axios
    .get("api/cart/", getConfig(getState))
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: CART_LOADED,
        payload: res.data,
      });
      dispatch({
        type: CART_DISPLAY,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const applyCouponCode = (couponCode) => (dispatch) => {
  if (couponCode == "") {
    dispatch(returnErrors({ non_field_errors: ["Invalid Coupon"] }, 400));
    return;
  }
  axios
    .get(`api/discount/${couponCode}`, getConfig())
    .then((res) => {
      dispatch({
        type: NO_ERRORS,
      });
      dispatch({
        type: DISCOUNT_APPLIED,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: DISCOUNT_RESET,
      });
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};
